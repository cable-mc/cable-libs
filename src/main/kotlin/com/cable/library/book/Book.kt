package com.cable.library.book

import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.inventory.EquipmentSlotType
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.CompoundNBT
import net.minecraft.nbt.ListNBT
import net.minecraft.nbt.StringNBT
import net.minecraft.util.Hand
import net.minecraft.util.text.IFormattableTextComponent
import net.minecraft.util.text.ITextComponent

fun of(vararg pages: IFormattableTextComponent): Book {
    return book(*pages)
}

fun book(vararg pages: IFormattableTextComponent): Book {
    val book = ItemStack(Items.WRITTEN_BOOK, 1)
    val nbt = CompoundNBT()
    val pagesNBT = ListNBT()
    for (page in pages) {
        pagesNBT.add(StringNBT.valueOf(ITextComponent.Serializer.toJson(page)))
    }
    nbt.put("pages", pagesNBT)
    nbt.put("author", StringNBT.valueOf("Cable"))
    nbt.put("title", StringNBT.valueOf("Book"))
    book.tag = nbt
    return Book(book)
}

fun sendBook(player: ServerPlayerEntity, vararg pages: IFormattableTextComponent) = of(*pages).open(player)

class Book(val itemStack: ItemStack) {
    fun open(player: ServerPlayerEntity) {
        val previousItem = player.getItemBySlot(EquipmentSlotType.MAINHAND)
        player.setItemSlot(EquipmentSlotType.MAINHAND, itemStack)
        player.openItemGui(itemStack, Hand.MAIN_HAND)
        player.setItemSlot(EquipmentSlotType.MAINHAND, previousItem)
    }
}
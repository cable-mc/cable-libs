package com.cable.library.event

import com.cable.library.reflect.grab
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.eventbus.ListenerList
import net.minecraftforge.eventbus.api.Event
import net.minecraftforge.eventbus.api.EventPriority
import net.minecraftforge.eventbus.api.IEventBus
import net.minecraftforge.eventbus.api.IEventListener

public class EventSubscription(
    private val busID: Int,
    private val priority: EventPriority,
    private val listeners: ListenerList,
    public val listener: IEventListener,
    private val stopActions: MutableList<() -> Unit> = mutableListOf()
) {
    var stopped = false

    fun stop() {
        if (!stopped) {
            listeners.unregister(busID, listener)
            stopActions.forEach { it() }
            stopped = true
        }
    }

    fun start() {
        listeners.register(busID, priority, listener)
    }

    fun onStop(action: () -> Unit): EventSubscription {
        stopActions.add(action)
        return this
    }
}

inline fun <reified T : Event> subscribe(
    eventBus: IEventBus = MinecraftForge.EVENT_BUS,
    priority: EventPriority = EventPriority.NORMAL,
    crossinline handler: (event: T) -> Unit
): EventSubscription {
    return subscribeFor(
        times = -1,
        eventBus = eventBus,
        handler = handler,
        endHandler = {},
        priority = priority
    )
}

inline fun <reified T : Event> subscribeOnce(
    eventBus: IEventBus = MinecraftForge.EVENT_BUS,
    priority: EventPriority = EventPriority.NORMAL,
    crossinline condition: (event: T) -> Boolean = { true },
    crossinline handler: (event: T) -> Unit
): EventSubscription {
    return subscribeFor(
        eventBus = eventBus,
        priority = priority,
        times = 1,
        condition = condition,
        handler = handler,
        endHandler = {}
    )
}

inline fun <reified T : Event> subscribeFor(
    times: Int,
    eventBus: IEventBus = MinecraftForge.EVENT_BUS,
    priority: EventPriority = EventPriority.NORMAL,
    crossinline condition: (event: T) -> Boolean = { true },
    crossinline handler: (event: T) -> Unit = {},
    crossinline endHandler: () -> Unit
): EventSubscription {
    var executedTimes = 0
    val busID = eventBus.grab<Int>("busID")
    lateinit var subscription: EventSubscription
    val listener = IEventListener { event ->
        if ((!event.isCancelable || !event.isCanceled) && event is T) {
            if (condition(event)) {
                handler(event)
                executedTimes++
                if (executedTimes >= times && times != -1) {
                    subscription.stop()
                    endHandler()
                }
            }
        }
    }
    subscription = EventSubscription(busID, priority, Event().listenerList, listener)
    subscription.start()
    return subscription
}
package com.cable.library.player

import com.mojang.brigadier.Command.SINGLE_SUCCESS
import com.mojang.brigadier.builder.ArgumentBuilder
import com.mojang.brigadier.context.CommandContext
import java.util.UUID
import net.minecraft.command.CommandSource
import net.minecraft.command.ICommandSource
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.util.text.IFormattableTextComponent
import net.minecraft.util.text.ITextComponent
import net.minecraftforge.server.permission.PermissionAPI

public fun ServerPlayerEntity.isInvisibleTo(other: ServerPlayerEntity) : Boolean {
    return if (!this.isInvisible) {
        false
    } else if (other.isSpectator) {
        false
    } else {
        val team = this.team
        team == null || other.team !== team || !team.canSeeFriendlyInvisibles()
    }
}

public val NIL_UUID: UUID = UUID(0L, 0L)

public fun ICommandSource.hasPermissionNode(node: String): Boolean = canUseCommand(4, node)
public fun ICommandSource.sendMessage(message: IFormattableTextComponent): Unit = sendMessage(message, NIL_UUID)

public fun ICommandSource.canUseCommand(level: Int, node: String): Boolean {
    return if (this !is ServerPlayerEntity) {
        true
    } else {
        this.hasPermissions(level) || PermissionAPI.hasPermission(this, node)
    }
}

public fun CommandSource.hasPermission(node: String, level: Int = 4): Boolean {
    if (this.hasPermission(level)) {
        return true
    }
    val entity = entity ?: return false
    return entity is ServerPlayerEntity && entity.hasPermissionNode(node)
}

public fun CommandSource.returnFailure(text: ITextComponent): Int {
    sendFailure(text)
    return 0
}

public fun CommandSource.returnSuccess(text: ITextComponent, sendToOps: Boolean = false): Int {
    sendSuccess(text, sendToOps)
    return SINGLE_SUCCESS
}

public fun <T : ArgumentBuilder<CommandSource, T>> ArgumentBuilder<CommandSource, T>.executesSuccessfully(body: (CommandContext<CommandSource>) -> Unit): T {
    return executes {
        try {
            body(it)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        SINGLE_SUCCESS
    }
}
package com.cable.library.bossbar

import com.cable.library.event.EventSubscription
import com.cable.library.event.subscribeFor
import com.cable.library.text.text
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.util.math.MathHelper
import net.minecraft.util.text.IFormattableTextComponent
import net.minecraft.world.BossInfo
import net.minecraft.world.server.ServerBossInfo
import net.minecraftforge.event.TickEvent
import net.minecraftforge.fml.LogicalSide
import kotlin.math.ceil
import kotlin.math.floor

public fun createBossBar(
    title: IFormattableTextComponent,
    colour: BossInfo.Color = BossInfo.Color.BLUE,
    percentage: Float = 100f
): ServerBossInfo {
    return ServerBossInfo(
        title, colour, BossInfo.Overlay.PROGRESS
    ).also { it.percent = floor(percentage / 100f) }
}

public fun ServerBossInfo.setPlayers(players: List<ServerPlayerEntity>): ServerBossInfo {
    this.players.filterNot { players.contains(it) }.forEach { removePlayer(it) }
    players.filterNot { players.contains(it) }.forEach { addPlayer(it) }
    return this
}

public fun ServerBossInfo.countdown(
    seconds: Int,
    title: IFormattableTextComponent = "".text(),
    action: (bossBar: ServerBossInfo) -> Unit
): EventSubscription {
    var remainingTicks = seconds * 20
    fun updateName() {
        if (title.string.replace("§r", "").isNotBlank()) {
            this.name = text(
                title.string
                    .replace("MINUTES", ceil((remainingTicks / 20f) / 60f).toInt().toString())
                    .replace("SECONDS", ceil(remainingTicks / 20f).toInt().toString())
            )
        }
    }

    updateName()
    return subscribeFor<TickEvent.ServerTickEvent>(
        times = remainingTicks,
        condition = { it.phase == TickEvent.Phase.END && it.side == LogicalSide.SERVER },
        handler = {
            remainingTicks--
            val ratio = remainingTicks.toFloat() / (seconds * 20)
            percent = MathHelper.clamp(ratio, 0f, 1f)
            updateName()
        },
        endHandler = { action(this) }
    )
}

public fun ServerBossInfo.stop() {
    isVisible = false
}
package com.cable.library.reflect

import java.lang.reflect.Field

public fun Class<*>.getAllFieldsWithAnnotation(annotation: Class<out Annotation>): MutableList<Field> {
    val fields = if (superclass !== Object::class.java && superclass != null) superclass.getAllFieldsWithAnnotation(annotation) else mutableListOf()
    fields.addAll(declaredFields.filter { it.isAnnotationPresent(annotation) }.map { it.isAccessible = true ; it }.toMutableList())
    return fields
}

public fun Class<*>.getAllDeclaredFields(): MutableList<Field> {
    val fields = if (superclass !== Object::class.java) superclass.getAllDeclaredFields() else mutableListOf()
    fields.addAll(declaredFields)
    return fields
}

public fun Class<*>.getFieldNamed(name: String): Field? {
    return getAllDeclaredFields().find { it.name == name }
}

public inline fun <reified T> Any.grab(fieldName: String): T {
    val field = this::class.java.getDeclaredField(fieldName)
    field.isAccessible = true
    return field.get(this) as T
}

public inline fun <T, R> T.into(block: (T) -> R): R = block(this)
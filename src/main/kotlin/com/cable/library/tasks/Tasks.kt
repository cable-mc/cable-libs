package com.cable.library.tasks

import com.cable.library.CableLibs.server
import java.util.concurrent.CompletableFuture

/**
 * Simple shortcut to the Task builder for simple, delayed, synchronous actions.
 */
public fun after(seconds: Number, action: () -> Unit): Any = Task.builder()
        .delay((seconds.toDouble()*20.0).toLong())
        .execute(action)
        .build()

public fun <T> async(block: () -> T): CompletableFuture<T> = CompletableFuture.supplyAsync(block)
public fun <T> sync(block: () -> T): CompletableFuture<T> {
    val future = CompletableFuture<T>()
    server.execute { future.complete(block()) }
    return future
}
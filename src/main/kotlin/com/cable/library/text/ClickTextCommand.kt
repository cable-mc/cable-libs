package com.cable.library.text

import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.arguments.StringArgumentType
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import java.util.UUID
import net.minecraft.command.CommandSource
import net.minecraft.command.Commands
import net.minecraft.entity.player.ServerPlayerEntity

public fun register(dispatcher: CommandDispatcher<CommandSource>) {
    dispatcher.register(
        LiteralArgumentBuilder.literal<CommandSource>("clicktext")
            .requires {src -> src.entity is ServerPlayerEntity}
            .then(
                Commands.argument("text-uuid", StringArgumentType.string())
                    .executes { ctx ->
                        val player = ctx.source.playerOrException
                        handlers[UUID.fromString(ctx.getArgument("text-uuid", String::class.java))]?.accept(player)
                        return@executes 1
                    }
            )
    )
}

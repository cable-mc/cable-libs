package com.cable.library.text

import java.util.UUID
import java.util.function.Consumer
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.util.text.Color
import net.minecraft.util.text.IFormattableTextComponent
import net.minecraft.util.text.StringTextComponent
import net.minecraft.util.text.Style
import net.minecraft.util.text.TextFormatting
import net.minecraft.util.text.event.ClickEvent
import net.minecraft.util.text.event.HoverEvent

public class Text internal constructor() {

    public companion object {
        internal fun resolveComponent(text: Any): IFormattableTextComponent {
            return StringTextComponent(text.toString().replace("&[A-Fa-f0-9k-oK-oRr]".toRegex()) { "§${it.value.substring(1)}" })
        }
    }

    private var style = StringTextComponent("").style // Accessing directly broke in dependent projects
    private var head: IFormattableTextComponent? = null

    public fun parse(vararg components: Any): IFormattableTextComponent {
        components.forEach {
            when (it) {
                is IFormattableTextComponent -> {
                    addComponent(it)
                    //it.style.setParentStyle(style)
                    style = getBlankStyle()
                }
                is ClickEvent -> style = style.withClickEvent(it)
                is HoverEvent -> style = style.withHoverEvent(it)
                is TextFormatting -> {
                    when {
                        it.isColor -> style = style.withColor(Color.fromRgb(it.color!!))
                        it == TextFormatting.UNDERLINE -> style = style.setUnderlined(true)
                        it == TextFormatting.BOLD -> style = style.withBold(true)
                        it == TextFormatting.ITALIC -> style = style.withItalic(true)
                        it == TextFormatting.OBFUSCATED -> style = style.setObfuscated(true)
                        it == TextFormatting.RESET -> style = Style.EMPTY
                    }
                }
                else -> addComponent(resolveComponent(it).also { it.style = it.style.applyTo(style) })
            }
        }

        return head?: StringTextComponent("Empty!")
    }

    private fun addComponent(component: IFormattableTextComponent) {
        if (head == null) {
            head = component
            component.style.assimilate(style)
            style = getBlankStyle()
            // Removed because of(hover(), blue()) would remove the hover
//            component.style.setParentStyle(style.createDeepCopy())
//            style = getBlankStyle()
        } else {
            head?.add(component.also{it.style.assimilate(style)})
        }
    }

    private fun getBlankStyle() = StringTextComponent("").style.withBold(false).withItalic(false).setUnderlined(false).setObfuscated(false).withColor(
        Color.fromRgb(TextFormatting.WHITE.color!!)).withClickEvent(null).withHoverEvent(null)
}

public fun of(vararg components: Any): IFormattableTextComponent = text(*components)
public fun text(vararg components: Any): IFormattableTextComponent = Text().parse(*components)

public val handlers = hashMapOf<UUID, Consumer<ServerPlayerEntity>>()

public fun click(action: Consumer<ServerPlayerEntity>): ClickEvent {
    val uuid = UUID.randomUUID()
    handlers[uuid] = action
    return ClickEvent(ClickEvent.Action.RUN_COMMAND, "/clicktext $uuid")
}

public fun click(action: (p: ServerPlayerEntity) -> Any): ClickEvent = click(Consumer { action.invoke(it) })

public fun hover(text: IFormattableTextComponent): HoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, text)

public fun hover(text: String): HoverEvent = hover(Text.resolveComponent(text))

public fun hover(item: ItemStack): HoverEvent = HoverEvent(HoverEvent.Action.SHOW_ITEM, HoverEvent.ItemHover(item))

public fun hover(entity: LivingEntity) = HoverEvent(HoverEvent.Action.SHOW_ENTITY, HoverEvent.EntityHover(entity.type, entity.uuid, entity.displayName))
public fun String.red() = of(this).also { it.style = it.style.withColor(TextFormatting.RED.toJavaColor()) }
public fun String.black() = of(this).also { it.style = it.style.withColor(TextFormatting.BLACK.toJavaColor()) }
public fun String.darkBlue() = of(this).also { it.style = it.style.withColor(TextFormatting.DARK_BLUE.toJavaColor()) }
public fun String.darkGreen() = of(this).also { it.style = it.style.withColor(TextFormatting.DARK_GREEN.toJavaColor()) }
public fun String.darkAqua() = of(this).also { it.style = it.style.withColor(TextFormatting.DARK_AQUA.toJavaColor()) }
public fun String.darkRed() = of(this).also { it.style = it.style.withColor(TextFormatting.DARK_RED.toJavaColor()) }
public fun String.darkPurple() = of(this).also { it.style = it.style.withColor(TextFormatting.DARK_PURPLE.toJavaColor()) }
public fun String.gold() = of(this).also { it.style = it.style.withColor(TextFormatting.GOLD.toJavaColor()) }
public fun String.gray() = of(this).also { it.style = it.style.withColor(TextFormatting.GRAY.toJavaColor()) }
public fun String.darkGray() = of(this).also { it.style = it.style.withColor(TextFormatting.DARK_GRAY.toJavaColor()) }
public fun String.blue() = of(this).also { it.style = it.style.withColor(TextFormatting.BLUE.toJavaColor()) }
public fun String.green() = of(this).also { it.style = it.style.withColor(TextFormatting.GREEN.toJavaColor()) }
public fun String.aqua() = of(this).also { it.style = it.style.withColor(TextFormatting.AQUA.toJavaColor()) }
public fun String.lightPurple() = of(this).also { it.style = it.style.withColor(TextFormatting.LIGHT_PURPLE.toJavaColor()) }
public fun String.yellow() = of(this).also { it.style = it.style.withColor(TextFormatting.YELLOW.toJavaColor()) }
public fun String.white() = of(this).also { it.style = it.style.withColor(TextFormatting.WHITE.toJavaColor()) }

public fun String.text() = of(this)
public fun String.stripCodes(): String = this.replace("[&§][A-Ea-e0-9K-Ok-oRr]".toRegex(), "")

public fun IFormattableTextComponent.onClick(action: (p: ServerPlayerEntity) -> Unit): IFormattableTextComponent = onClick(Consumer { action.invoke(it) })
public fun IFormattableTextComponent.onClick(action: Consumer<ServerPlayerEntity>) = also { it.style = it.style.withClickEvent(click(action)) }
public fun IFormattableTextComponent.onHover(string: String) = also { it.style = it.style.withHoverEvent(hover(string)) }
public fun IFormattableTextComponent.onHover(text: IFormattableTextComponent) = also { it.style = it.style.withHoverEvent(hover(text)) }
public fun IFormattableTextComponent.underline() = also { it.style = it.style.setUnderlined(true) }
public fun IFormattableTextComponent.bold() = also { it.style = it.style.withBold(true) }
public fun IFormattableTextComponent.italicise() = also { it.style = it.style.withItalic(true) }
public fun IFormattableTextComponent.strikethrough() = also { it.style = it.style.setStrikethrough(true) }
public fun IFormattableTextComponent.obfuscate() = also { it.style = it.style.setObfuscated(true) }

public fun IFormattableTextComponent.add(other: IFormattableTextComponent): IFormattableTextComponent {
    this.append(other) //todo idk if this will work
    // TODO it did not work
    return this;
}

public operator fun IFormattableTextComponent.plus(other: IFormattableTextComponent) = add(other)

public fun IFormattableTextComponent.add(string: String): IFormattableTextComponent {
    this.add(string.text())
    return this;
}
public fun TextFormatting.toJavaColor(): Color = Color.fromRgb(this.color!!)

public fun Style.assimilate(other: Style): Style {
    return this.withHoverEvent(hoverEvent ?: other.hoverEvent).withClickEvent(clickEvent ?: other.clickEvent).withColor(color ?: other.color)
}

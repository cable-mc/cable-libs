package com.cable.library;

import com.cable.library.text.ClickTextCommandKt;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cable.library.CableLibs.MOD_ID;

@Mod(
    value = MOD_ID
)
public class CableLibs {

    public static CableLibs INSTANCE;
    public static MinecraftServer server;

    public CableLibs() {
        INSTANCE = this;
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initialize);
    }

    public void initialize(FMLCommonSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(this);
    }
    public static final String MOD_ID = "cablelibs";

    private final Logger logger = Logger.getLogger("Cable-Libs");

    public Logger getLogger() {
        return logger;
    }

    @SubscribeEvent
    public void onRegisterCommands(RegisterCommandsEvent event) {
        ClickTextCommandKt.register(event.getDispatcher());
    }


    public static void logError(String error) {
        INSTANCE.logger.log(Level.SEVERE, error);
    }

    public static void logWarn(String warn) {
        INSTANCE.logger.log(Level.WARNING, warn);
    }

    public static void logInfo(String info) {
        INSTANCE.logger.log(Level.INFO, info);
    }

}

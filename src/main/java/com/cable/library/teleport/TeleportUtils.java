package com.cable.library.teleport;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

public class TeleportUtils {
    public static void teleport(ServerPlayerEntity player, World world, BlockPos pos) {
        teleport(player, (ServerWorld) world, pos, player.yRot, player.xRot);
    }

    public static void teleport(ServerPlayerEntity player, ServerWorld world, BlockPos pos, float yaw, float pitch) {
        teleport(player, world, pos, yaw);
    }

    public static void teleport(ServerPlayerEntity player, ServerWorld to, BlockPos pos, float yaw) {
        teleport(player, to, pos, yaw);
    }

    public static void teleport(ServerPlayerEntity player, ServerWorld to, Vector3d pos, float yaw, float pitch) {
        player.teleportTo(to, pos.x, pos.y, pos.z, yaw, pitch);
    }
}

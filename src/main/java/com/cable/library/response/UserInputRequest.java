package com.cable.library.response;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

public class UserInputRequest<T> implements IUserInputRequest<T> {

	private ServerPlayerEntity player;
	private IFormattableTextComponent requestMessage, invalidResponseMessage;
	private Map<String, T> choices;
	private Consumer<IResponseData<T>> responseBehaviour;

	private UserInputRequest(@Nonnull UserInputRequest.Builder<T> builder, @Nonnull ServerPlayerEntity player){
		this.requestMessage = builder.requestMessage;
		this.invalidResponseMessage = builder.invalidResponseMessage;
		this.choices = builder.choices;
		this.responseBehaviour = Objects.requireNonNull(builder.responseBehaviour, "response behaviour must not be null");
		this.player = Objects.requireNonNull(player, "player must not be null");
		process();
	}

	@Override
	public ServerPlayerEntity getPlayer() {
		return player;
	}

	@Override
	public Optional<IFormattableTextComponent> getRequestMessage() {
		return Optional.ofNullable(requestMessage);
	}

	@Override
	public Optional<IFormattableTextComponent> getInvalidResponseMessage() {
		return Optional.ofNullable(invalidResponseMessage);
	}

	@Override
	public void sendInvalidResponseMessage() {
		if(invalidResponseMessage != null){
			player.sendMessage(invalidResponseMessage, new UUID(0, 0));
		}
	}

	@Override
	public Optional<Map<String, T>> getChoices() {
		return Optional.ofNullable(choices);
	}

	private void onResponse(IResponseData<T> response) {
		responseBehaviour.accept(response);
	}

	private void process() {
		if(requestMessage != null){
			player.sendMessage(requestMessage, new UUID(0, 0));
		}
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public void cancel() {
		MinecraftForge.EVENT_BUS.unregister(this);
	}

	@SubscribeEvent
	public void onMessage(ServerChatEvent event){
		if(event.getPlayer() == player){
			event.setCanceled(true);
			IResponseData<T> response = new ResponseData<>(this, event.getPlayer(), event.getMessage(), choices);
			onResponse(response);
		}
	}

	public static class Builder<T> implements IUserInputRequest.Builder<T> {

		private IFormattableTextComponent requestMessage, invalidResponseMessage;
		private Map<String, T> choices;
		private Consumer<IResponseData<T>> responseBehaviour;

		@Override
		public Builder<T> requestMessage(@Nullable IFormattableTextComponent message) {
			this.requestMessage = message;
			return this;
		}

		@Override
		public Builder<T> invalidResponseMessage(@Nullable IFormattableTextComponent message) {
			this.invalidResponseMessage = message;
			return this;
		}

		@Override
		public Builder<T> choices(@Nullable Map<String, T> choices) {
			this.choices = choices;
			return this;
		}

		@Override
		public IUserInputRequest.Builder<T> onResponse(@Nonnull Consumer<IResponseData<T>> responseBehaviour) {
			this.responseBehaviour = responseBehaviour;
			return this;
		}

		@Override
		public IUserInputRequest<T> send(@Nonnull ServerPlayerEntity player) {
			Objects.requireNonNull(player, "player must not be null");
			Objects.requireNonNull(responseBehaviour, "response behaviour must not be null");
			return new UserInputRequest<>(this, player);
		}

	}

}
